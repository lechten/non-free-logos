<!--- Local IspellDict: en -->

This repository contains non-free logos of organizations of which I am
a member.  The fact that I use those logos (and embed them in CC
licensed OER) does not grant any rights to you.
